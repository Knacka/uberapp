'use strict';

var uberControllers = angular.module('uberControllers', []);

uberControllers.controller('PostListCtrl', ['$scope',
  function($scope) {
    $scope.posts = "Massa posts"
  }]);

uberControllers.controller('PostDetailsCtrl', ['$scope', 'Post', function($scope, Post) {
	$scope.postDetails = "Massa postsdetaljer";
	$scope.postJson = Post.query();
}]);

