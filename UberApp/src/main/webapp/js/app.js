'use strict';
var uberApp = angular.module('uberApp', ['uberControllers', 'ngRoute','uberServices']);

uberApp.config(['$routeProvider', function($routeProvider){
	$routeProvider.
		when('/posts', {
			templateUrl: 'partials/post-list.html',
			controller: 'PostListCtrl'
		}).
		when('/posts/:postId',{
			templateUrl: 'partials/post-details.html',
			controller: 'PostDetailsCtrl'
		}).
		otherwise({
			redirectTo: '/posts'
		});
}])