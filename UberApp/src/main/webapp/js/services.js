'use strict';

/* Services */

var uberServices = angular.module('uberServices', ['ngResource']);

uberServices.factory('Post', ['$resource',
  function($resource){
    return $resource('resources/posts.json', {}, {
      query: {method:'GET', isArray:false}
    });
  }]);